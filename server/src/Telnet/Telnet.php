<?php

namespace Telnet;

class Telnet {

    const TELNET_STATUS_ERROR = false;
	const TELNET_STATUS_OK = true;

    private string $host;
	private int $port;
    private int $errno = null; // Error code
	private string $errstr = null; // Error message
    private float $connect_timeout; // Timeout to connect to remote
    private $socket;

    public function __construct(string $host = '127.0.0.1', int $port = 8080, float $connect_timeout = 1.0) {
        $this->host = $host;

        if (!is_int($port)) {
			throw new \InvalidArgumentException('port must be int');
		}

        $this->port = $port;

        $this->setConnectTimeout($connect_timeout);
    }

    /**
	 * @return string the hostname to connect to
	 */
	public function getHostname(): string {
		return $this->host;
	}

    // public function getConnectTimeout(): float {
	// 	return $this->connectTimeout;
	// }

    // public function setConnectTimeout(float $connect_timeout) {
	// 	if (!(is_float($connect_timeout) && $connect_timeout >= 0.0))
	// 		throw new \InvalidArgumentException('Connection timeout must be float');

	// 	$this->connect_timeout = $connect_timeout;
	// }

    /**
	 * Attempts connection to remote host.
	 *
	 * @return boolean true if successful
	 * @throws ConnectionException on error
	 */
    public function connect() {
        $this->socket = socket_create(AF_INET, SOCK_STREAM, 0) or throw new ConnectionException("Cannot create socket");

        if (!$this->socket) 
            throw new ConnectionException("Cannot create socket");

        $result = socket_bind($this->socket, $this->host,  $this->port)

        if (!$result)
            throw new ConnectionException("Cannot bind to socket");

        $result = socket_listen($this->socket, 3)

        if (!$result)
            throw new ConnectionException("Cannot set up socket listener");
        // $this->fp = fsockopen($url, $this->port,  $this->errno, $this->errstr, $this->connect_timeout);

        // if ($this->fp === false)
		// 	throw new ConnectionException("Cannot connect to $this->host on port $this->port");

        return self::TELNET_STATUS_OK;  
    }

    /**
	 * Closes socket connection
	 *
	 * @return boolean
	 */
    public function disconnect(): bool {
		// clean up resources
		socket_close($this->socket);

        return self::TELNET_OK;
	}
}